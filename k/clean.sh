#! /bin/bash

find . -name "*~" -exec rm -f {} \;

find . -name "*-kompiled" -exec rm -rf {} \;

find . -name ".k" -type d -exec rm -rf {} \;