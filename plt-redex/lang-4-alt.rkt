#lang racket

(require redex)

(define-language lang-4-alt
  (N number)
  (E N
     (+ N N ...)
     (if0 E E E)
     (r E))
  (M (control E reg N))
  (C hole
     (if0 C E E)
     (r C)
     (control C reg N)))

(define
  lang-4-alt-red
  (reduction-relation
   lang-4-alt
   (--> (in-hole C (+ N_1 N_2 N_3 N_4 ...))
        (in-hole C (+ ,(+ (term N_1) (term N_2)) N_3 N_4 ...)))
   (--> (in-hole C (+ N_1 N_2))
        (in-hole C ,(+ (term N_1) (term N_2))))
   (--> (in-hole C (if0 0
                        E_1
                        E_2))
        (in-hole C E_1))
   (--> (in-hole C (if0 N_1
                        E_1
                        E_2))
        (in-hole C E_2)
        (side-condition (not (equal? (term N_1) 0))))
   (--> (in-hole (control C reg N_2) (r N_1))
        (in-hole (control C reg N_1) N_1))))